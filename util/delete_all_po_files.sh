#!/bin/bash

# configuration laden
FILE=xgettext_variablen.sh
if test -f "$FILE"; then
    # . xgettext_variablen.sh
    source $FILE
fi

verzeichnisse=$(find $localefolder -maxdepth 1 -type d)    # verzeichnisse als string
dirs_slash="${verzeichnisse//${localefolder}/}"
dirs="${dirs_slash//\//}"
sprachen=($(echo "$dirs"))  # sprachen als array

cd $localefolder;
whereami=$(pwd)

if [[ "$whereami" == "$localefolder" ]]
then
  # echo "INFO :: We are in FOLDER $localefolder !"
  # echo "INFO :: We have SPRACHEN : ${sprachen[*]} !"

  for item in ${sprachen[*]}
  do

    lang=${item:0:2}
    langdir=$localefolder/$lang


    if [ "$lang" != "$ausgangssprache" ];
    then
        echo -e "INFO :: Wir suchen in LANG $lang nach '$messagespo' !"

        # wir erzeugen ein unterverzeichnis mit dem namen der sprache
        if [[ ! -d "$langdir" ]]
        then
          mkdir -v "$langdir"
        fi

        # Wir Erzeugen das unterverzeichnis LC_MESSAGES wenn es fehlt
        if [[ ! -d "$langdir/$lcmessages/" ]]
        then
          mkdir -v "$langdir/$lcmessages/"
        fi

                # Wir Erzeugen das unterverzeichnis LC_MESSAGES wenn es fehlt
        if [[ -f "$langdir/$lcmessages/$messagespo" ]]
        then
          rm -fv "$langdir/$lcmessages/$messagespo" || echo -e "can't remove write-protected $langdir/$lcmessages/$messagespo"
        fi

    fi

  done

  # zuletzt kopieren wir die po datei in den hauptordner
  # cp "$localefolder/$ausgangssprache/$lcmessages/$messagespo" "$localefolder/$messagespo"

#git add .
#git commit -m "new translation"
#git push tax.de master
else
 echo "ERROR :: We are not in FOLDER $localefolder !"
fi
