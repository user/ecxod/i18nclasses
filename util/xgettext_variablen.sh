#!/bin/bash
# configurationsfile für tax.de (xgettext)

# BEISPIEL:
# Wir haben folgende Pfade:
# /home/user/webspace/www.example.com/_php/classen    <--- hier sind classen drin
# /home/user/webspace/www.example.com/_php/pages      <--- hier sind die Seiten drin
# /home/user/webspace/www.example.com/webroot/locale/de/LC_MESSAGES/messages.po

domain="www.example.com"
basis="/home/user/webspace/$domain"       # Sollte das home-verzeichnis des users sein
phproot="_php/classen"
phppages="_php/pages"
webroot="webroot"                         # /home/user/webspace/$domain/$webroot ist DOCUMENT_ROOT oder CONTEXT_DOCUMENT_ROOT im webserver
localefolder="$basis/$webroot/locale"     # bindtextdomain( $domain, $localefolder ); 
ausgangssprache="de"
lcmessages="LC_MESSAGES"
messagespo="messages.po"
