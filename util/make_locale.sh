#!/bin/bash

# configuration laden
whereami=$(pwd)
FILE="$whereami/xgettext_variablen.sh"
if test -f "$FILE"; then
    # . xgettext_variablen.sh
    source $FILE
else
    echo -e "ERROR :: Konnte Variablen nicht laden! $FILE"
    exit 0
fi

verzeichnisse=$(find $localefolder -maxdepth 1 -type d)    # verzeichnisse als string
dirs_slash="${verzeichnisse//${localefolder}/}"
dirs="${dirs_slash//\//}"
sprachen=($(echo "$dirs"))  # sprachen als array

cd $localefolder
whereami=$(pwd)
echo -e "start aus $whereami"

if [[ "$whereami" == "$localefolder" ]]
then
  # echo "INFO :: We are in FOLDER $localefolder !"
  # echo "INFO :: We have SPRACHEN : ${sprachen[*]} !"

  for item in ${sprachen[*]}
  do

    lang=${item:0:2}
    langdir=$localefolder/$lang


    if [ "$lang" != "$ausgangssprache" ];
    then
        echo "INFO :: We check LANGUAGE $lang !"

        if [[ ! -d "$langdir" ]]
        then
          mkdir -v "$langdir"
        fi

        # Wir Erzeugen das unterverzeichnis LC_MESSAGES wenn es fehlt
        if [[ ! -d "$langdir/$lcmessages/" ]]
        then
          mkdir -v "$langdir/$lcmessages/"
          # touch "$langdir/$lcmessages/$messagespo"
        fi

        # Wir schreiben ein leere po file wenn sie fehlt
        if [[ -f "$langdir/$lcmessages/$messagespo" ]]
        then
          # po files existiert
          xgettext $basis/$phproot/* $basis/$phppages/* $basis/$webroot/* \
            --join-existing \
            --omit-header \
            --no-location \
            --add-comments \
            --from-code=UTF-8 \
            --package-name=$packagename \
            --package-version=v1.0  \
            --msgid-bugs-address=c@zp1.net \
            --output-dir=$langdir/$lcmessages \
            --verbose

        else
          # po file fehlt
          xgettext $basis/$phproot/* $basis/$phppages/* $basis/$webroot/* \
            --add-comments \
            --no-location \
            --from-code=UTF-8 \
            --package-name=$packagename \
            --package-version=v1.0  \
            --msgid-bugs-address=c@zp1.net \
            --output-dir=$langdir/$lcmessages \
            --verbose
        fi

    fi

  done

  # zuletzt kopieren wir die po datei in den hauptordner
  # cp "$localefolder/$ausgangssprache/$lcmessages/$messagespo" "$localefolder/$messagespo"

git add -v .
git commit -v -m "new translation"
git push -v tax.de master
else
 echo -e "ERROR :: We are not in FOLDER $localefolder !"
 whereami=$(pwd)
 echo  -e "We are in Folder $whereami."
fi

