<?php

declare(strict_types=1);

namespace Ecxod\I18n;

class CSS
{

    public const BSCOLORS = [ 
        "0" => "transparent",
        "1" => "primary",
        "2" => "secondary",
        "3" => "success",
        "4" => "danger",
        "5" => "warning",
        "6" => "info",
        "7" => "light",
        "8" => "dark",
        "9" => "white",
    ];

}
